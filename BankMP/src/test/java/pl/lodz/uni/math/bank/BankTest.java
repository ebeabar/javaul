package pl.lodz.uni.math.bank;

import java.util.Random;

import org.junit.Test;

import pl.lodz.uni.math.bank.data.Account;
import pl.lodz.uni.math.bank.data.Address;
import pl.lodz.uni.math.bank.data.Client;
import pl.lodz.uni.math.bank.exception.TransactinHasAlreadyBeenCompleted;
import pl.lodz.uni.math.bank.exception.TransactionHasNotBeenCompleted;
import pl.lodz.uni.math.bank.transaction.Deposit;
import pl.lodz.uni.math.bank.transaction.Transaction;
import pl.lodz.uni.math.bank.transaction.Transfer;
import pl.lodz.uni.math.bank.transaction.Withdraw;

public class BankTest {

	@Test
	public void test_bank() {

		Address bankAddress = new Address("Lodz", "lodzkie", "90-001", "Pilsudskiego", 5);
		Bank bank = Bank.getInstance();
		bank.init("mBank", bankAddress);

		Address clientAddress = new Address("Lodz", "lodzkie", "90-553", "Kopernika", 23);
		Client client = new Client("Anna", "Kowalska", "600 100 234", clientAddress);
		Account account = new Account("personal account", "PL74 7114 0000 0574 0785 27793 5155");
		client.addAccount(account);
		bank.addClient(client);

		Client client2 = new Client("Beata", "Nowak", "+48 602 123 456", clientAddress);
		Account account2 = new Account("comapny account", "PL74 1114 0000 0609 4406 87269 4375");
		client2.addAccount(account2);
		bank.addClient(client2);

		System.out.println(bank);

		Deposit deposit = new Deposit(account, 1500);
		bank.addTransaction(deposit);

		Transaction deposit2 = new Deposit(account, 500);
		bank.addTransaction(deposit2);

		Transfer transfer = new Transfer(account, account2, 200);
		bank.addTransaction(transfer);

		try {
			bank.deleteTransaction(deposit);
			bank.stopTransaction(deposit2);
		} catch (TransactionHasNotBeenCompleted | TransactinHasAlreadyBeenCompleted e) {
			e.printStackTrace();
		}

		bank.charge();

		try {
			bank.deleteTransaction(deposit);
			bank.stopTransaction(deposit2);
		} catch (TransactionHasNotBeenCompleted | TransactinHasAlreadyBeenCompleted e) {
			e.printStackTrace();
		}

		Random rn = new Random();
		Withdraw withdraw;
		for (int i = 0; i < 10; i++) {
			deposit = new Deposit(account, rn.nextInt(1000000));
			withdraw = new Withdraw(account, rn.nextInt(1000000));
			transfer = new Transfer(account, account2, rn.nextInt(10000));
			bank.addTransaction(withdraw);
			bank.addTransaction(deposit);
			bank.addTransaction(transfer);
		}

		bank.charge();

		client.printAccountHistory(account);
		System.out.println(bank);
		// System.out.println(account);
		// System.out.println(account2);

		System.out.println(client.getAccountHistory("PL74 7114 0000 0574 0785 27793 5155"));
		// System.out.println(client.getAccountHistory("PL74 7114 0000 0574 0785
		// 27793 5152"));
	}

}
