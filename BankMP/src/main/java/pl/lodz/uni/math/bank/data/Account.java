package pl.lodz.uni.math.bank.data;

import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.HashMap;

public class Account {
	private String name;
	private String number;
	private double balance = 0;
	private AtomicInteger counter = new AtomicInteger(0);
	private Map<Integer, String> transactions = new HashMap<Integer, String>();

	public Account(String name, String number) {
		this.name = name;
		this.number = number;
	}

	public String toString() {
		String str = "Account: " + this.name + " " + this.number + " balance: " + this.balance + "\n";
		for (Entry<Integer, String> entry : transactions.entrySet()) {
			str += entry.getKey() + "\t" + entry.getValue() + "\n";
		}
		return str;
	}

	public void addAmountToBalance(double amount) {
		this.balance += amount;
		transactions.put(getNextUniqueIndex(), "+" + amount);
	}

	public void removeAmountFromBalance(double amount) {
		this.balance -= amount;
		transactions.put(getNextUniqueIndex(), "-" + amount);
	}

	public double getBalance() {
		return balance;
	}

	public String getNumber() {
		return number;
	}

	public String getName() {
		return name;
	}

	public int getNextUniqueIndex() {
		return counter.getAndIncrement();
	}

	public Map<Integer, String> getTransactions() {
		return transactions;
	}

}
