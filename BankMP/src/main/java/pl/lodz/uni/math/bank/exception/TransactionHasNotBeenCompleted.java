package pl.lodz.uni.math.bank.exception;

public class TransactionHasNotBeenCompleted extends Exception {

	private static final long serialVersionUID = 1L;

	public TransactionHasNotBeenCompleted() {
		super();
	}
	
	public TransactionHasNotBeenCompleted(String msg) {
		super(msg);
	}
}
