package pl.lodz.uni.math.bank.transaction;

import pl.lodz.uni.math.bank.exception.InsufficientFundsException;

public interface Transaction {
	void doTransaction() throws InsufficientFundsException;
	void stopTransaction();
	void deleteTransaction();
}
