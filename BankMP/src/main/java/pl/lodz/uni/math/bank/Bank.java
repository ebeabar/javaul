package pl.lodz.uni.math.bank;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import pl.lodz.uni.math.bank.data.Address;
import pl.lodz.uni.math.bank.data.Client;
import pl.lodz.uni.math.bank.exception.InsufficientFundsException;
import pl.lodz.uni.math.bank.exception.TransactinHasAlreadyBeenCompleted;
import pl.lodz.uni.math.bank.exception.TransactionHasNotBeenCompleted;
import pl.lodz.uni.math.bank.transaction.Transaction;

public class Bank {
	private String name;
	private Address address;
	private List<Client> clients = new ArrayList<>();
	private List<Transaction> pendingTransaction = new ArrayList<>();
	private List<Transaction> completedTransaction = new ArrayList<>();
	private List<Transaction> discardedTransaction = new ArrayList<>();

	private static Bank instance = null;

	private Bank() {
	}

	public static Bank getInstance() {
		if (instance == null) {
			instance = new Bank();
		}
		return instance;
	}

	public void init(String name, Address address) {
		this.name = name;
		this.address = address;
	}

	public void addClient(Client client) {
		clients.add(client);
	}

	public String toString() {
		String str = "";
		str += "Name of the bank: " + this.name + "\n";
		str += address.toString() + "\n\n";

		for (Client i : clients) {
			str += i.toString();
		}
		return str;
	}

	public void addTransaction(Transaction transaction) {
		pendingTransaction.add(transaction);
	}

	public void charge() {
		Iterator<Transaction> iter = pendingTransaction.iterator();
		while (iter.hasNext()) {
			Transaction trans = iter.next();
			try {
				trans.doTransaction();
				completedTransaction.add(trans);
			} catch (InsufficientFundsException e) {
				discardedTransaction.add(trans);
				System.out.println("Transfer declined insufficient amount on account " + e.getAccount().getNumber()
						+ " balance: " + e.getAccount().getBalance());
			} finally {
				iter.remove();
			}
		}
	}

	public void deleteTransaction(Transaction name) throws TransactionHasNotBeenCompleted {
		if (completedTransaction.contains(name)) {
			name.deleteTransaction();
			completedTransaction.remove(name);
		} else {
			throw new TransactionHasNotBeenCompleted();
		}
	}

	public void stopTransaction(Transaction name) throws TransactinHasAlreadyBeenCompleted {
		if (pendingTransaction.contains(name)) {
			name.stopTransaction();
			pendingTransaction.remove(name);
		} else {
			throw new TransactinHasAlreadyBeenCompleted();
		}
	}

}
