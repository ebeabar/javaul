package pl.lodz.uni.math.bank.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Client {

	private String firstName;
	private String lastName;
	private String telephoneNumber;
	private Address address;
	private List<Account> accounts = new ArrayList<>();

	public Map<Integer, String> getAccountHistory(String accountNumber) {
		Account result = accounts.stream().filter(a -> accountNumber.equals(a.getNumber())).findAny().orElse(null);
		if (result == null) {
			return null;
		}
		return result.getTransactions();
	}

	public void printAccountHistory(Account account) {
		System.out.println(account);
	}

	public Client(String firstName, String lastName, String telephone, Address address) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.telephoneNumber = telephone;
		this.address = address;
	}

	public void addAccount(Account account) {
		accounts.add(account);
	}

	public String toString() {
		String str = "Name of the client: " + this.firstName + " " + this.lastName + "\nTelephone: "
				+ this.telephoneNumber + "\n";
		str += address.toString() + "\n";
		if (accounts != null) {
			for (Account i : accounts) {
				str += i.toString() + "\n";
			}
		}
		return str;
	}

}
