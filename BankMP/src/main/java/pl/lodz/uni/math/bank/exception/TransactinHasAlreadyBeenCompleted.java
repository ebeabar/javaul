package pl.lodz.uni.math.bank.exception;

public class TransactinHasAlreadyBeenCompleted extends Exception {

	private static final long serialVersionUID = 1L;

	public TransactinHasAlreadyBeenCompleted() {
		super();
	}
	
	public TransactinHasAlreadyBeenCompleted(String msg) {
		super(msg);
	}
	
}
