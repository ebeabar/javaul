package pl.lodz.uni.math.bank.transaction;

import pl.lodz.uni.math.bank.data.Account;

public class Deposit implements Transaction {
	private Account accountTo;
	private double amount;

	public Deposit(Account accountTo, double amount) {
		this.accountTo = accountTo;
		this.amount = amount;
	}

	@Override
	public void doTransaction() {
		accountTo.addAmountToBalance(amount);
	}

	@Override
	public void stopTransaction() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteTransaction() {
		accountTo.removeAmountFromBalance(amount);

	}

}
