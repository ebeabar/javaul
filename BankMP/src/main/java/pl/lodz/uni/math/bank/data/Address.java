package pl.lodz.uni.math.bank.data;

public class Address {
	private String city;
	private String state;
	private String zip;
	private String street;
	private int number;

	public Address(String city, String state, String zip, String street, int number) {
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.street = street;
		this.number = number;
	}

	public String toString() {
		return "Address: " + this.city + " " + this.state + " " + this.zip + " " + this.street + " " + this.number;
	}

}
