package pl.lodz.uni.math.bank.transaction;

import pl.lodz.uni.math.bank.data.Account;
import pl.lodz.uni.math.bank.exception.InsufficientFundsException;

public class Transfer implements Transaction {
	private Account accountTo;
	private Account accountFrom;
	private double amount;

	public Transfer(Account accountFrom, Account accountTo, double amount) {
		this.accountTo = accountTo;
		this.accountFrom = accountFrom;
		this.amount = amount;
	}

	@Override
	public void doTransaction() throws InsufficientFundsException {
		if (accountFrom.getBalance() >= amount) {
			accountFrom.removeAmountFromBalance(amount);
			accountTo.addAmountToBalance(amount);
		} else {
		   double needs = amount - accountTo.getBalance();
	         throw new InsufficientFundsException(needs,accountFrom);
		}
	}

	@Override
	public void stopTransaction() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteTransaction() {
		accountFrom.addAmountToBalance(amount);
		accountTo.removeAmountFromBalance(amount);
	}

}
