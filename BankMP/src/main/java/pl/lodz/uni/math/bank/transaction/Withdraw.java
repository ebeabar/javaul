package pl.lodz.uni.math.bank.transaction;

import pl.lodz.uni.math.bank.data.Account;
import pl.lodz.uni.math.bank.exception.InsufficientFundsException;

public class Withdraw implements Transaction {
   private Account accountFrom;
   private double amount;

   public Withdraw(Account account, double amount) {
      this.accountFrom = account;
      this.amount = amount;
   };

   @Override
   public void doTransaction() throws InsufficientFundsException {
      if (accountFrom.getBalance() >= amount) {
    	  accountFrom.removeAmountFromBalance(amount);
      } else {
         double needs = amount - accountFrom.getBalance();
         throw new InsufficientFundsException(needs,accountFrom);
      }
   }

   @Override
   public void stopTransaction() {
      throw new UnsupportedOperationException();
   }

   @Override
   public void deleteTransaction() {
	   accountFrom.addAmountToBalance(amount);

   }

}
