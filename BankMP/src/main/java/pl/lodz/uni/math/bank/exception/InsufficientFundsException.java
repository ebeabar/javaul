package pl.lodz.uni.math.bank.exception;

import pl.lodz.uni.math.bank.data.Account;

public class InsufficientFundsException extends Exception {

   private static final long serialVersionUID = 1L;
   private double amount;
   private Account account;

   public InsufficientFundsException(double amount, Account account) {
      this.amount = amount;
      this.account = account;
   }

   public double getAmount() {
      return amount;
   }

   public Account getAccount() {
      return account;
   }

}
