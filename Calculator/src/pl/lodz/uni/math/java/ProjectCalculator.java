package pl.lodz.uni.math.java;

public class ProjectCalculator {

	public static void main(String[] args) {
		Calculator calculator = new Calculator();
		System.out.println(calculator.add(5, 3));
		System.out.println(calculator.sqrt(25));
	}

}
