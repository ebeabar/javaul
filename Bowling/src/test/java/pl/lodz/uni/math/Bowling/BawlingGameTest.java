package pl.lodz.uni.math.Bowling;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BawlingGameTest {

   private static final int MAX_FRAMES = 10;
   private static final int MAX_ROLLS = 20;
   private static final int MAX_SCORE = 300;
   private Bowling bowling;

   @Before
   public void setUp() {
      bowling = new BowlingImpl();
      bowling.startGame();
   }

   @Test
   public void allStrike() {
      for (int i = 0; i < MAX_FRAMES; i++) {
         bowling.roll(10);
      }
      bowling.roll(10);
      bowling.roll(10);
      assertTrue(bowling.getScore() == MAX_SCORE);
   }

   @Test
   public void allSpare() {
      for (int i = 0; i < MAX_ROLLS; i++) {
         bowling.roll(5);
      }
      bowling.roll(5);
      assertTrue(bowling.getScore() == 150);
   }

   @Test
   public void oneSpare() {
      bowling.roll(5);
      bowling.roll(5);
      bowling.roll(3);
      for (int i = 0; i < MAX_ROLLS - 3; i++) {
         bowling.roll(0);
      }

      assertTrue(bowling.getScore() == 16);
   }

   @Test
   public void oneStrike() {
      bowling.roll(10); // as 2 rolls
      bowling.roll(3);
      bowling.roll(4);
      for (int i = 0; i < MAX_ROLLS - 4; i++) {
         bowling.roll(0);
      }

      assertTrue(bowling.getScore() == 24);
   }

   @Test
   public void framesWithoutSparesAndStrikes() {
      for (int i = 0; i < MAX_ROLLS; i++) {
         bowling.roll(3);
      }
      assertTrue(bowling.getScore() == 60);
   }

   @Test
   public void minScore() {
      for (int i = 0; i < MAX_ROLLS; i++) {
         bowling.roll(0);
      }
      assertTrue(bowling.getScore() == 0);
   }

   @Test
   public void allOnes() {
      for (int i = 0; i < MAX_ROLLS; i++) {
         bowling.roll(1);
      }
      assertTrue(bowling.getScore() == 20);
   }

   @Test
   public void strikeInFirstAttempt() {
      bowling.roll(10);
      bowling.roll(5);
      for (int i = 0; i < MAX_ROLLS - 3; i++) {
         bowling.roll(0);
      }
      // System.out.println(bowling.getScore());
      assertTrue(bowling.getScore() == 20);
   }

   @Test
   public void strikeInSecondAttempt() {
      bowling.roll(0);
      bowling.roll(0);
      bowling.roll(10);
      bowling.roll(5);
      for (int i = 0; i < MAX_ROLLS - 5; i++) {
         bowling.roll(0);
      }
      // System.out.println(bowling.getScore());
      assertTrue(bowling.getScore() == 20);
   }

   @Test
   public void strikeInLastAttempt() {
      for (int i = 0; i < MAX_ROLLS - 2; i++) {
         bowling.roll(0);
      }
      bowling.roll(10);
      bowling.roll(5);
      bowling.roll(5);
      assertTrue(bowling.getScore() == 20);
   }

   @Test
   public void spareInLastAttempt() {
      for (int i = 0; i < MAX_ROLLS - 2; i++) {
         bowling.roll(0);
      }
      bowling.roll(5);
      bowling.roll(5);
      bowling.roll(5);
      assertTrue(bowling.getScore() == 15);
   }

   @Test(expected = GameEndedException.class)
   public void tooManyMovesInAllStrikes() {
      for (int i = 0; i < MAX_FRAMES; i++) {
         bowling.roll(10);
      }
      bowling.roll(10);
      bowling.roll(10);
      bowling.roll(10); // additional move
   }

   @Test(expected = GameEndedException.class)
   public void tooManyMovesInAllSpare() {
      for (int i = 0; i < MAX_ROLLS; i++) {
         bowling.roll(5);
      }
      bowling.roll(5);
      bowling.roll(3); // additional move
   }

   @Test(expected = GameEndedException.class)
   public void tooManyMoves() {
      for (int i = 0; i < MAX_ROLLS; i++) {
         bowling.roll(4);
      }
      bowling.roll(2); // additional move
   }

   @Test(expected = WrongNumberOfPins.class)
   public void wrongNumberOfPins() {
      bowling.roll(11);
   }

}
