package pl.lodz.uni.math.Bowling;

public class GameEndedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public GameEndedException() {
		super();
	}

	public GameEndedException(String message) {
		super(message);
	}

}
