package pl.lodz.uni.math.Bowling;

public class WrongNumberOfPins extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public WrongNumberOfPins() {
		super();
	}

	public WrongNumberOfPins(String message) {
		super(message);
	}

}
