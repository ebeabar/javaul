package pl.lodz.uni.math.Bowling;

import java.util.ArrayList;
import java.util.List;

public class BowlingImpl implements Bowling {

	private static final int ZERO = 0;
	private static final int MAX_PINS = 10;
	private static final int MAX_FRAMES = 10;
	private boolean bonus = false;

	private List<Frame> frames = new ArrayList<>();

	@Override
	public void startGame() {
		frames.clear();
	}

	public void addNewRollToFrame(int n) {
		frames.add(new Frame(n));
	}

	public Frame getCurrentFrame() {
		return frames.get(frames.size() - 1);
	}

	public Frame getPreviousFrame() {
		return frames.get(frames.size() - 2);
	}

	public void addSecondRollToFrame(int n) {
		getCurrentFrame().completeFrame(n);
	}

	public void isSecondRollValid(int n) {
		if (!isSecondRollInRange(n)) {
			throw new WrongNumberOfPins();
		}

		addSecondRollToFrame(n);
	}

	private boolean isSecondRollInRange(int n) {
		return (n <= MAX_PINS - getCurrentFrame().getFirstShoot());
	}

	public void roll(int numberOfPins) {

		validateNumberOfPins(numberOfPins);

		initalizeFrame(numberOfPins);

		if (isCurrentFrameCompleted()) {
			recalculateScore();
		}

		checkIfBonusNeeded();

		checkIfGameHasEnded();
	}

	private void initalizeFrame(int numberOfPins) {
		if (!bonus) {
			if (isFirstRoll() || isCurrentFrameCompleted()) {
				addNewRollToFrame(numberOfPins);
			} else {
				isSecondRollValid(numberOfPins);
			}
		} else {
			getCurrentFrame().initalizeBonusFrame(numberOfPins);
		}
	}

	private void validateNumberOfPins(int numberOfPins) {
		if (!isInRange(numberOfPins)) {
			throw new WrongNumberOfPins();
		}
	}

	private void checkIfBonusNeeded() {
		if (frames.size() == MAX_FRAMES) {
			Frame frame = getCurrentFrame();
			if (frame.isStrike()) {
				frames.add(Frame.createBonusFrame(RoundStatus.STRIKE));
				bonus = true;
			} else if (frame.isSpare()) {
				frames.add(Frame.createBonusFrame(RoundStatus.SPARE));
				bonus = true;
			} else {
				bonus = false;
			}
		}
	}

	private void checkIfGameHasEnded() {
		if (!bonus && frames.size() > MAX_FRAMES) {
			throw new GameEndedException();
		}
	}

	private void recalculateScore() {
		for (int i = 0; i < frames.size(); i++) {
			Frame frame = frames.get(i);
			if (frame.isRecalculationRequired()) {
				recalculateFrame(frame, i);
			}
		}
	}

	private void recalculateFrame(Frame frame, int index) {
		if (shouldRecalculationBeDone(frame)) {
			checkIfRecalculationPossible(frame, index + 1);
		}

		if (frame.isRecalculationRequired()) {
			checkIfRecalculationPossible(frame, index + 2);
		}
	}

	private void checkIfRecalculationPossible(Frame frame, int index) {
		Frame nextFrame;
		if (frames.size() > index) {
			nextFrame = frames.get(index);
			recalculateFrameValue(frame, nextFrame);
		}
	}

	private boolean shouldRecalculationBeDone(Frame frame) {
		return (frame.isStrike() && frame.areTwoRecalculatesNeeded()) || frame.isSpare();
	}

	private void recalculateFrameValue(Frame current, Frame next) {
		int remainingValues = current.getRemainingCalculateValues();

		if (next.isStrike()) {
			addResultToFrame(current, next.getResult(), 1);
		} else if (onlyOneValueFromSpareNeeded(remainingValues)) {
			addResultToFrame(current, next.getFirstShoot(), 1);
		} else if (next.isBonusSpare()) {
			addResultToFrame(current, next.getBonusResultSpare(), 2);
		} else if (next.isBonusStrike()) {
			addResultToFrame(current, next.getBonusResultStrike(), 2);
		} else {
			addResultToFrame(current, next.getResult(), 2);
		}
	}

	private boolean onlyOneValueFromSpareNeeded(int remainingValues) {
		return remainingValues == 1;
	}

	private void addResultToFrame(Frame frame, int value, int decrease) {
		frame.addBonusResult(value);
		frame.decreaseRemainingCalculateValues(decrease);
	}

	private boolean isCurrentFrameCompleted() {
		return getCurrentFrame().isCompleted();
	}

	private boolean isFirstRoll() {
		return frames.size() == 0;
	}

	private boolean isInRange(int numberOfPins) {
		return numberOfPins >= ZERO && numberOfPins <= MAX_PINS;
	}

	public int getScore() {
		return frames.stream().mapToInt(Frame::getResult).sum();
	}

	@Override
	public void printScore() {
		for (int i = 0; i < frames.size(); i++) {
			Frame frame = frames.get(i);
			System.out.println("Round " + (i + 1) + " " + frame);
		}

	}

}
