package pl.lodz.uni.math.Bowling;

public enum RoundStatus {
	STRIKE, SPARE, PINS_REMAINING, ONE_ROLL, BONUS_STRIKE, BONUS_SPARE;
}
