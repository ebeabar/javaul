package pl.lodz.uni.math.Bowling;

public class Frame {

	private static final int MAX_PINS = 10;
	private Integer firstShoot;
	private Integer secondShoot;
	private int result;
	private RoundStatus status;
	private int remainingCalculateValues = 0;

	public int getRemainingCalculateValues() {
		return remainingCalculateValues;
	}

	public boolean areTwoRecalculatesNeeded() {
		return remainingCalculateValues == 2;
	}

	private Frame(RoundStatus stat) {
		status = stat;
		remainingCalculateValues = 0;
	}

	public Frame(int fs) {
		firstShoot = fs;
		result = firstShoot;
		status = getStatus();
		if (firstShoot == MAX_PINS) {
			remainingCalculateValues = 2;
		}
	}

	public void initalizeBonusFrame(int value) {
		if (status == RoundStatus.BONUS_SPARE) {
			initializeBonusSpare(value);
		} else if (status == RoundStatus.BONUS_STRIKE) {
			initializeBonusStrike(value);
		}
	}

	private void initializeBonusSpare(int value) {
		if (firstShoot == null) {
			firstShoot = value;
		} else {
			throw new GameEndedException();
		}
	}

	private void initializeBonusStrike(int value) {
		if (firstShoot == null) {
			firstShoot = value;
		} else if (secondShoot == null) {
			secondShoot = value;
		} else {
			throw new GameEndedException();
		}
	}

	public int getBonusResultStrike() {
		return firstShoot + secondShoot;
	}

	public int getBonusResultSpare() {
		return firstShoot;
	}

	public static Frame createBonusFrame(RoundStatus status) {
		if (status == RoundStatus.STRIKE) {
			return new Frame(RoundStatus.BONUS_STRIKE);
		} else if (status == RoundStatus.SPARE) {
			return new Frame(RoundStatus.BONUS_SPARE);
		}
		return null;
	}

	public void completeFrame(int ss) {
		secondShoot = ss;
		result += secondShoot;
		status = getStatus();
		if (firstShoot + secondShoot == MAX_PINS) {
			remainingCalculateValues = 1;
		}
	}

	public void decreaseRemainingCalculateValues(int value) {
		remainingCalculateValues -= value;
	}

	public RoundStatus getStatus() {
		if (firstShoot == MAX_PINS) {
			return RoundStatus.STRIKE;
		}

		if (secondShoot == null) {
			return RoundStatus.ONE_ROLL;
		}

		if (firstShoot + secondShoot == MAX_PINS) {
			return RoundStatus.SPARE;
		}

		return RoundStatus.PINS_REMAINING;

	}

	public void addBonusResult(int bonus) {
		this.result += bonus;
	}

	public boolean isFrameCompleted() {
		return !status.equals(RoundStatus.ONE_ROLL);
	}

	public void setFirstShoot(int firstShoot) {
		this.firstShoot = firstShoot;
	}

	public String toString() {
		String str = "";
		if (firstShoot != null) {
			str += firstShoot;
		}
		if (secondShoot != null) {
			str += "/" + secondShoot;
		}
		str += " Result: " + result + " remainingCalculate " + remainingCalculateValues;
		return str;
	}

	public boolean isStrike() {
		return status.equals(RoundStatus.STRIKE);
	}

	public boolean isSpare() {
		return status.equals(RoundStatus.SPARE);
	}

	public boolean isBonusStrike() {
		return status.equals(RoundStatus.BONUS_STRIKE);
	}

	public boolean isBonusSpare() {
		return status.equals(RoundStatus.BONUS_SPARE);
	}

	public boolean isCompleted() {
		return !status.equals(RoundStatus.ONE_ROLL) && isBonusSpareCompleted() && isBonusStrikeCompleted();
	}

	public boolean isBonusSpareCompleted() {
		if (status == RoundStatus.BONUS_SPARE && firstShoot == null) {
			return false;
		}
		return true;
	}

	public boolean isBonusStrikeCompleted() {
		if (status == RoundStatus.BONUS_STRIKE && (firstShoot == null || secondShoot == null)) {
			return false;
		}
		return true;
	}

	public int getFirstShoot() {
		return firstShoot;
	}

	public boolean isRecalculationRequired() {
		return remainingCalculateValues != 0;
	}

	public int getResult() {
		return result;
	}

}