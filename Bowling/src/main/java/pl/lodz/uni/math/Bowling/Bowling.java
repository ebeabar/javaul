package pl.lodz.uni.math.Bowling;

public interface Bowling {

   public void roll(int numberOfPins);

   public void printScore();
   
   public void startGame();

   public int getScore();

}
