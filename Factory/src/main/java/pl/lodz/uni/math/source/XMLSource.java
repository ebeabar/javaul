package pl.lodz.uni.math.source;

import java.util.List;

import pl.lodz.uni.math.data.User;

public class XMLSource implements Source {

	private volatile static XMLSource instance;

	private XMLSource() {
	}

	public static XMLSource getInstance() {
		if (instance == null) {
			synchronized (XMLSource.class) {
				if (instance == null) {
					instance = new XMLSource();
				}
			}
		}

		return instance;
	}

	public User selectUserById(int User) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<User> selectAllUsers() {
		// TODO Auto-generated method stub
		return null;
	}

}
