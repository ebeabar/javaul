package pl.lodz.uni.math.source;

import java.util.List;

import pl.lodz.uni.math.data.User;

public interface Source {

	User selectUserById(int User);

	List<User> selectAllUsers();

}
