package pl.lodz.uni.math.source;

import java.util.List;

import pl.lodz.uni.math.data.User;

public class WebServiceSource implements Source {

	private volatile static WebServiceSource instance;

	private WebServiceSource() {
	}

	public static WebServiceSource getInstance() {
		if (instance == null) {
			synchronized (WebServiceSource.class) {
				if (instance == null) {
					instance = new WebServiceSource();
				}
			}
		}

		return instance;
	}

	public User selectUserById(int User) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<User> selectAllUsers() {
		// TODO Auto-generated method stub
		return null;
	}

}
