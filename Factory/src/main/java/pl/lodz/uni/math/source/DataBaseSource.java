package pl.lodz.uni.math.source;

import java.util.List;

import pl.lodz.uni.math.data.User;

public class DataBaseSource implements Source {

	private volatile static DataBaseSource instance;

	private DataBaseSource() {
	}

	public static DataBaseSource getInstance() {
		if (instance == null) {
			synchronized (DataBaseSource.class) {
				if (instance == null) {
					instance = new DataBaseSource();
				}
			}
		}

		return instance;
	}

	public User selectUserById(int User) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<User> selectAllUsers() {
		// TODO Auto-generated method stub
		return null;
	}

}
