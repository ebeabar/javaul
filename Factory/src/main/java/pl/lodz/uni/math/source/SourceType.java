package pl.lodz.uni.math.source;

public enum SourceType {

   XML, DATABASE, WEBSERVICE;

}
