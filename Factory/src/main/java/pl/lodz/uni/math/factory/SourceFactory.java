package pl.lodz.uni.math.factory;

import java.util.List;

import pl.lodz.uni.math.data.User;
import pl.lodz.uni.math.source.DataBaseSource;
import pl.lodz.uni.math.source.Source;
import pl.lodz.uni.math.source.SourceType;
import pl.lodz.uni.math.source.WebServiceSource;
import pl.lodz.uni.math.source.XMLSource;

public class SourceFactory implements Source {

   private volatile static SourceFactory instance;

   private SourceFactory() {
   }

   public static SourceFactory getInstance() {
      if (instance == null) {
         synchronized (SourceFactory.class) {
            if (instance == null) {
               instance = new SourceFactory();
            }
         }
      }

      return instance;
   }

   private static Source source = null;

   public void setSourceOfData(SourceType sourceType) {
      switch (sourceType) {
      case DATABASE:
         source = DataBaseSource.getInstance();
         break;
      case WEBSERVICE:
         source = WebServiceSource.getInstance();
         break;
      case XML:
         source = XMLSource.getInstance();
         break;
      default:
         throw new IllegalArgumentException("Type unsporrted " + source);
      }
   }

   @Override
   public User selectUserById(int id) {
      return source.selectUserById(id);
   }

   @Override
   public List<User> selectAllUsers() {
      return source.selectAllUsers();
   }

}
