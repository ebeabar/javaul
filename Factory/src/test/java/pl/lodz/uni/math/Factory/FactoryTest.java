package pl.lodz.uni.math.Factory;

import pl.lodz.uni.math.factory.SourceFactory;
import pl.lodz.uni.math.source.SourceType;

import org.junit.Test;

public class FactoryTest {

	@Test
	public void testFactory() {

		SourceFactory factory = SourceFactory.getInstance();

		factory.setSourceOfData(SourceType.XML);

		factory.selectAllUsers();
		factory.selectUserById(2);

		factory.setSourceOfData(SourceType.WEBSERVICE);

		factory.selectAllUsers();
		factory.selectUserById(2);
	}
}
