package pl.lodz.uni.math;

public class Singleton {
   private static Singleton instance = null; //new Singleton();

   private Singleton() {
      System.out.println("konstruktor");
   };

   public static void getInstance() {
      System.out.println("instance");
      //return instance;
   }

   private int x;

   public int getX() {
      return x;
   }

   public void setX(int x) {
      this.x = x;
   }

   static {
      System.out.println("blok statyczny");
   }

   {
      System.out.println("blok inicjujacy");
   }
}
