package pl.lodz.uni.math;

public class TeachingConstructor extends BaseConstructor implements GlobalConstructor {

	public OtherConstructor other;

	public void CoolFunction() {
	};

	public TeachingConstructor(int x) {
		System.out.println("Block arg TC");
	};

	public TeachingConstructor() {
		System.out.println("Block default TC");
		other = new OtherConstructor();
	};

	{
		System.out.println("Block free TC");
	}

	static {
		System.out.println("Block static TC");
	}
}
