package pl.lodz.uni.math;

public class BaseConstructor {
	OtherConstructor other;

	public BaseConstructor() {
		System.out.println("Blok default BC");
		other = new OtherConstructor();
	}

	{
		System.out.println("Blok free BC");
	}

	static {
		System.out.println("Blok static BC");
	}

}
