package pl.lodz.uni.math;

public class Main {

	public static void main(String[] args) {
		System.out.println("Global Constructor new TC: ");
		GlobalConstructor tc = new TeachingConstructor(2);
		
		System.out.println("\nBaseConstructor new BC: ");
		//BaseConstructor gc = new BaseConstructor();
		
		System.out.println("\nTeaching Constructor new TC: ");
		//TeachingConstructor tc2 = new TeachingConstructor(2);
	}

}
