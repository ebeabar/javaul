package pl.lodz.uni.math;

public class Client {

	private String firstName;
	private String lastName;
	private Address address;
	private Account[] accounts;

	public Client(String firstName, String lastName, Address address) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
	}

	public void addAccount(Account account) {
		if (this.accounts == null) {
			this.accounts = new Account[1];
		} else {
			Account[] temp = new Account[this.accounts.length+1];
			System.arraycopy(this.accounts, 0, temp, 0, this.accounts.length);
			this.accounts = temp;
		}
		this.accounts[this.accounts.length-1] = account;
	}

	public String toString() {
		String str = "";
		str += "Name of the client: " + this.firstName + " " + this.lastName + "\n";
		str += address.toString() + "\n";
		if (accounts != null) {
			for (Account i : accounts) {
				str += i.toString() + "\n";
			}
		}
		return str;
	}

}
