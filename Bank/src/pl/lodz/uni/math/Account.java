package pl.lodz.uni.math;

public class Account {
   private String name;
   private String number;
   private double balance = 0;

   public Account(String name, String number) {
      this.name = name;
      this.number = number;
   }

   public String toString() {
      return "Account: " + this.name + " " + this.number + " " + this.balance; 
   }

   public void addAmountToBalance(double amount) {
      this.balance += amount;
   }

   public void removeAmountFromBalance(double amount) {
      this.balance -= amount;
   }

   public double getBalance() {
      return balance;
   }

   public String getNumber() {
      return number;
   }

}
