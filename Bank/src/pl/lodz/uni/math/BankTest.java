package pl.lodz.uni.math;

public class BankTest {

   public static void main(String[] args) {

      Address bankAddress = new Address("Lodz", "lodzkie", "90-001", "Pilsudskiego", 5);
      Bank bank = Bank.getInstance();
      bank.init("mBank", bankAddress);

      Address clientAddress = new Address("Lodz", "lodzkie", "90-553", "Kopernika", 23);
      Client client = new Client("Anna", "Kowalska", clientAddress);
      Account account = new Account("personal account", "PL74 7114 0000 0574 0785 27793 5155");
      client.addAccount(account);
      bank.addClient(client);

      Client client2 = new Client("Beata", "Nowak", clientAddress);
      Account account2 = new Account("comapny account", "PL74 1114 0000 0609 4406 87269 4375");
      client2.addAccount(account2);
      bank.addClient(client2);

      System.out.println(bank);

      Deposit deposit = new Deposit(account, 1500);
      bank.addTransaction(deposit);

      Transaction deposit2 = new Deposit(account, 500);
      bank.addTransaction(deposit2);

      Transfer transfer = new Transfer(account2, account, "MB", 200);
      bank.addTransaction(transfer);

      bank.deleteTransaction(deposit);
      bank.stopTransaction(deposit2);
      
      bank.charge();
      
      bank.deleteTransaction(deposit);
      bank.stopTransaction(deposit2);
      
      System.out.println(account);
      System.out.println(account2);
   }

}
