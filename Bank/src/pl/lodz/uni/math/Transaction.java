package pl.lodz.uni.math;

public interface Transaction {
	public void doTransaction() throws InsufficientFundsException;
	public void stopTransaction();
	public void deleteTransaction();

}
