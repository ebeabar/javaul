package pl.lodz.uni.math;

public class Check implements Transaction {
   private Account accountTo;
   private double amount;

   public Check(Account account, double amount) {
      this.accountTo = account;
      this.amount = amount;
   };

   @Override
   public void doTransaction() throws InsufficientFundsException {
      if (accountTo.getBalance() >= amount) {
         accountTo.removeAmountFromBalance(amount);
      } else {
         double needs = amount - accountTo.getBalance();
         throw new InsufficientFundsException(needs,accountTo);
      }
   }

   @Override
   public void stopTransaction() {
      // TODO Auto-generated method stub

   }

   @Override
   public void deleteTransaction() {
      accountTo.addAmountToBalance(amount);

   }

}
