package pl.lodz.uni.math;

public class Transfer implements Transaction {
	private Account accountTo;
	private Account accountFrom;
	private String swift;
	private double amount;

	public Transfer(Account accountTo, Account accountFrom, String swift, double amount) {
		this.accountTo = accountTo;
		this.accountFrom = accountFrom;
		this.swift = swift;
		this.amount = amount;
	}

	@Override
	public void doTransaction() throws InsufficientFundsException {
		if (accountFrom.getBalance() >= amount) {
			accountFrom.removeAmountFromBalance(amount);
			accountTo.addAmountToBalance(amount);
		} else {
		   double needs = amount - accountTo.getBalance();
	         throw new InsufficientFundsException(needs,accountFrom);
		}
	}

	@Override
	public void stopTransaction() {
		// TODO Auto-generated method stub
	}

	@Override
	public void deleteTransaction() {
		accountFrom.addAmountToBalance(amount);
		accountTo.removeAmountFromBalance(amount);
	}

}
