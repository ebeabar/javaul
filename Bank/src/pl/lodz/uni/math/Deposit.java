package pl.lodz.uni.math;

public class Deposit implements Transaction {
	private Account accountFrom;
	private double amount;

	public Deposit(Account accountFrom, double amount) {
		this.accountFrom = accountFrom;
		this.amount = amount;
	}

	@Override
	public void doTransaction() {
		accountFrom.addAmountToBalance(amount);

	}

	@Override
	public void stopTransaction() {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteTransaction() {
		accountFrom.removeAmountFromBalance(amount);

	}

}
