package pl.lodz.uni.math;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Bank {
   private String name;
   private Address address;
   private Client[] clients;
   private List<Transaction> pendingTransaction = new ArrayList<>();
   private List<Transaction> completedTransaction = new ArrayList<>();
   private List<Transaction> discardedTransaction = new ArrayList<>();

   private static Bank instance = null;

   private Bank() {
   }

   public static Bank getInstance() {
      if (instance == null) {
         instance = new Bank();
      }
      return instance;
   }

   public void init(String name, Address address) {
      this.name = name;
      this.address = address;
   }

   public void addClient(Client client) {
      if (this.clients == null) {
         this.clients = new Client[1];
      } else {
         Client[] temp = new Client[this.clients.length + 1];
         System.arraycopy(this.clients, 0, temp, 0, this.clients.length);
         this.clients = temp;
      }
      this.clients[this.clients.length - 1] = client;
   }

   public String toString() {
      String str = "";
      str += "Name of the bank: " + this.name + "\n";
      str += address.toString() + "\n";

      if (clients != null) {
         for (Client i : clients) {
            str += i.toString() + "\n";
         }
      }
      return str;
   }

   public void addTransaction(Transaction transaction) {
      pendingTransaction.add(transaction);
   }

   public void charge() {
      Iterator<Transaction> iter = pendingTransaction.iterator();
      while (iter.hasNext()) {
         Transaction trans = iter.next();
         try {
            trans.doTransaction();
            completedTransaction.add(trans);
         } catch (InsufficientFundsException e) {
            discardedTransaction.add(trans);
            System.out.println("Transfer declined insufficient amount on account " + e.getAccount().getNumber()
                  + " balance: " + e.getAccount().getBalance());
         } finally {
            iter.remove();
         }
      }
   }
   
   public void deleteTransaction(Transaction name){
      if(completedTransaction.contains(name)){
         name.deleteTransaction();
         completedTransaction.remove(name);
      }
      else{
         System.out.println("Deleting this transaction is impossible");
      }    
   }
   
   public void stopTransaction(Transaction name){
      if(pendingTransaction.contains(name)){
         name.stopTransaction();
         pendingTransaction.remove(name);
      }
      else{
         System.out.println("Stopping this transaction is impossible");
      } 
   }

}
